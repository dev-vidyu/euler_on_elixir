defmodule EulerOnElixirTest do
  use ExUnit.Case
  doctest EulerOnElixir

  test "Should gratulate" do
    assert EulerOnElixir.hello() == :Euler
  end

  test "Problem 1" do
    assert EulerOnElixir.Problem1.solve(9) == 23
  end

  test "Problem 2" do
    assert EulerOnElixir.Problem2.solve(55) == 44
  end

  test "Problem 3" do
    assert EulerOnElixir.Problem3.solve(13195) == 29
  end
end
