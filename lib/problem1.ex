defmodule EulerOnElixir.Problem1 do
  @moduledoc """
  If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

  Find the sum of all the multiples of 3 or 5 below 1000.
  """

  @doc """
  Solves the problem

  """
  def solve(max) do
    Enum.reduce(1..max, 0, fn
      x, acc when rem(x, 3) == 0 or rem(x, 5) == 0 -> acc + x
      _x, acc -> acc
    end)
  end
end
