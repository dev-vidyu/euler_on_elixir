defmodule EulerOnElixir do
  @moduledoc """
  Project Euler solutions implemented with Elixir.
  """
  require EulerOnElixir.Problem1

  @doc """
  Hello world.

  ## Examples

      iex> EulerOnElixir.hello()
      :Euler

  """
  def hello do
    :Euler
  end
end
