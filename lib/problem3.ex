defmodule EulerOnElixir.Problem3 do
  @moduledoc """
  The prime factors of 13195 are 5, 7, 13 and 29.

  What is the largest prime factor of the number 600851475143 ?
  """

  defp prime_factors(num, divider) do
    case num do
      x when x <= 1 -> divider
      x when rem(x, divider) == 0 -> prime_factors(div(x, divider), divider)
      x -> prime_factors(x, divider + 1)
    end
  end

  @doc """
  Solves the problem

  """
  def solve(num) do
    prime_factors(num, 2)
  end
end
